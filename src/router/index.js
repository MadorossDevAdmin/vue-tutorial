import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/views/Main'
import Hello from '@/components/views/Hello'
import Test from '@/components/views/Test'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/hello',
      name: 'hello',
      component: Hello
    },
    {
      path: '/test',
      name: 'test',
      component: Test
    }
  ]
})

// Route Filter
// router.beforeEach((to, from, next) => {
//   if (to.matched.some(record =>record.meta.requiresAuth) && !auth.authenticated) {
//     next({ path: '/login', query: { redirect: to.fullPath }});
//   } else {
//     next();
//   }
// })
