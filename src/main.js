// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

//Using jQuery
import 'expose-loader?$!expose-loader?jQuery!jquery'
//Popper For Bootstrap marerial
import 'expose-loader?$!expose-loader?Popper!popper.js'
//Using FontAwesome
import 'font-awesome/css/font-awesome.min.css'

//Using Bootstrap material theme
import 'bootstrap-material-design'
import 'bootstrap-material-design/dist/css/bootstrap-material-design.min.css'


/* eslint-disable no-new */
const app = new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
